* Plantilla de captura para =org-mode=

La captura de información es una de las acciones más delicadas que
debemos hacer cuando trabajamos en el día a día. Muchas veces, esa
captura irá a la /agenda/ donde anotaremos tareas, reuniones, etc.

Este proyecto es una forma de mantener de forma ordenada las capturas
para mi agenda y como muestra por si a alguien más le puede servir
para adaptarlo a sus necesidades.
